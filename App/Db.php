<?php

namespace App;

class Db {

    use Singleton;

    protected $dbh;
    protected $sth;

    protected function __construct() {
        $config = Config::instance();
        $driver = $config->db["db"]["driver"];
        $host = $config->db["db"]["host"];
        $dbname = $config->db["db"]["dbname"];
        $user = $config->db["db"]["user"];
        $password = $config->db["db"]["password"];
        try {
            $this->dbh = new \PDO("{$driver}:host={$host};dbname={$dbname}", $user, $password);
        } catch(\PDOException $e) {
            throw new \App\Exceptions\Db($e->getMessage());
        }
    }

    public function execute($sql, $params = []) {
        $sth = $this->dbh->prepare($sql);
        $this->sth = $sth;
        $res = $sth->execute($params);
        if($res !== false) {
            return $res;
        } else {
            throw new \App\Exceptions\Db("Неверный SQL-запрос");
        }
    }

    public function query($sql, $params = [], $class) {
        $sth = $this->dbh->prepare($sql);
        $result = $sth->execute($params);
        if ($result !== false) {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        } else {
            throw new \App\Exceptions\Db("Неверный SQL-запрос");
        }
    }

    public function queryEach($sql, $params = [], $class) {
        $sth = $this->dbh->prepare($sql);
        $result = $sth->execute($params);
        if ($result !== false) {
            $sth->setFetchMode(\PDO::FETCH_CLASS, $class);
            function generate($sth) {
                while($row = $sth->fetch()) {
                    yield $row;
                }
            }
            return generate($sth);
        } else {
            throw new \App\Exceptions\Db("Неверный SQL-запрос");
        }
    }

    public function lastId() {
        return $this->dbh->lastInsertId();
    }

    public function rowCount() {
        return $this->sth->rowCount();
    }

}