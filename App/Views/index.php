<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            navProcessing();

            function navProcessing() {
                var buttons = ['.js-header',
                                '.js-create', 
                                '.js-sort',
                                '.js-one',
                                '.js-update',
                                '.js-delete',
                                '.js-pagination-next',
                                '.js-pagination-num',
                                '.js-pagination-back',
                                '.js-back']

                
                for (btn in buttons) {
                    $(buttons[btn]).click(function () {

                        if ($(this).attr('class') == 'js-sort') {
                            var btn_sort = $(this);
                        }

                        if ($(this).attr('class') == 'js-pagination-next' || $(this).attr('class') == 'js-pagination-back' || $(this).attr('class') == 'js-pagination-num') {
                            var btn_pagination = $(this);
                        }

                        var url = $(this).attr('href');

                        $.ajax({
                            url: url,
                            success: function (data) {
                                $('.js-content').html(data);

                                if (btn_pagination != undefined) {
                                    var href = $('.js-sort').attr("href").replace($('.js-sort').attr("href").substring($('.js-sort').attr("href").indexOf("?sort"), $('.js-sort').attr("href").indexOf("&")), '?sort='+sessionStorage['sort']);
                                    $('.js-sort').attr("href", href);
                                }

                                if (btn_sort != undefined) {
                                    if (sessionStorage['sort'] == "asc" || sessionStorage['sort'] == undefined) {
                                        var href = $('.js-sort').attr("href").replace($('.js-sort').attr("href").substring($('.js-sort').attr("href").indexOf("?sort")), "?sort=desc");
                                        $('.js-sort').attr("href", href + $('.js-sort').attr("data-sort"));
                                        sessionStorage['sort'] = 'desc';
                                        $('.js-sort').html("Сортировать &darr;");
                                    } else if (sessionStorage['sort'] == "desc") {
                                        var href = $('.js-sort').attr("href").replace($('.js-sort').attr("href").substring($('.js-sort').attr("href").indexOf("?sort")), "?sort=asc");
                                        $('.js-sort').attr("href", href + $('.js-sort').attr("data-sort"));
                                        sessionStorage['sort'] = 'asc';
                                        $('.js-sort').html("Сортировать &uarr;");
                                    }
                                }

                                return true;
                            }
                        });
                        return false;
                    });
                }
            }

        });
    </script>

</head>



<body class="js-content">

<div class="panel panel-default">
    <div class="panel-heading">
        <h1 style="text-align: center; cursor: pointer;" href="/index" class="js-header">Блог</h1>
    </div>

    <div class="panel-body">
        <a href="/create<?php echo '?page='.$page.'&sort='.$sort; ?>" class="js-create">+ Добавить новую запись</a>
    </div>
</div>

<div class="panel-body"><a href="/index?sort=desc<?php echo '&page='.$page; ?>" class="js-sort" data-sort="<?php echo '&page='.$page; ?>">Сортировать &darr;</a></div>

<?php foreach($posts as $article) : ?>
<div class="panel panel-default">

    <div class="panel-heading">
        <a href="/one<?php echo '?id='.$article->id.'&page='.$page.'&sort='.$sort; ?>" class="js-one"><?php echo $article->title; ?></a>
    </div>

    <div class="panel-heading"><?php echo $article->date; ?></div>

    <div class="panel-body">
        <?php if (!empty($article->img)): ?>
            <img src="<?php echo 'App/Views/img/'.$article->img; ?>" alt="img" class="img-responsive" height="200px" width="200px" />
        <?php endif; ?>

        <?php echo $article->content; ?>
    </div>

    <div class="panel-heading">
        <a href="/update<?php echo "?id=".$article->id.'&page='.$page.'&sort='.$sort; ?>" class="js-update">Редактировать</a> 
        ||
        <a href="/delete<?php echo "?id=".$article->id.'&page='.$page.'&sort='.$sort; ?>" class="js-delete">Удалить</a>
    </div>
</div>
<?php endforeach; ?>



<?php if ($pagination->total_pages() > 1) : ?>
    <footer class="alert alert-info">
        <?php
            // Ссылка на предыдущую страницу
            if ($pagination->has_previous_page()) {
                echo " <a href=\"/index?page=";
                echo $pagination->previous_page();
                echo "&sort=".$sort."\" class=\"js-pagination-back\">&laquo; Назад</a> ";
            }

            // Ссылки на конкретные страницы (весь список страниц)
            for ($i = 1; $i <= $pagination->total_pages(); $i++) {
                if ($i == $page) {
                    echo " <span class=\"selected\">{$i}</span> ";
                } else {
                    echo " <a href=\"/index?page={$i}&sort=".$sort."\" class=\"js-pagination-num\">{$i}</a> ";
                }
            }

            // Ссылка на следующую страницу
            if ($pagination->has_next_page()) {
                echo " <a href=\"/index?page=";
                echo $pagination->next_page();
                echo "&sort=".$sort."\" class=\"js-pagination-next\">Далее &raquo;</a> ";
            }
        ?>
    </footer>
<?php endif; ?>



<script src="App/Views/js/bootstrap.min.js"></script>

</body>

</html>