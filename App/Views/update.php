<?php if (!empty($errors)): foreach ($errors as $error): ?>
    <div class="alert alert-danger">
        <?php echo $error->getMessage(); ?>
    </div>
<?php endforeach; endif; ?>

<div class="panel panel-default">
    <div class="panel-heading"><h1 style="text-align: center;">Редактирование записи</h1></div>
    <div class="panel-heading">
        <a href="/index<?php echo '?page='.$page.'&sort='.$sort; ?>" class="js-back">&laquo; назад</a>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <form action="/update/" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="id" value="<?php echo (int)$_GET['id']; ?>">

            <div class="panel-heading">Заголовок:</div>
            <input type="text" name="title" value="<?php if(!empty($data->title)) echo $data->title; ?>" />
            <hr/>

            <div class="panel-heading">Текст:</div>
            <textarea name="content" cols="40" rows="8"><?php if(!empty($data->content)) echo $data->content; ?></textarea>
            <hr/>

            <div class="panel-heading">Изображение:</div>
            <?php if (!empty($data->img)): ?>
                <img src="<?php echo 'App/Views/img/'.$data->img; ?>" class="img-responsive" alt="img" height="200px" width="200px" />
            <?php endif; ?>
            <input type="hidden" name="img_old" value="<?php if(!empty($data->img)) echo $data->img; ?>">
            <input type="file" name="img" /> <br />
            
            <input type="submit" name="submit" value="Изменить" />
            <br/><br/>
        </form>
    </div>
</div>