<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>



<body class="panel-body">

<?php if(!empty($exception)): ?>
<div class="panel panel-default" style="background-color: oldlace">
    <h3 class="alert alert-danger">
        <?php echo $exception; ?>
    </h3>
</div>
<?php endif; ?>

<br />

<script src="App/Views/js/bootstrap.min.js"></script>

</body>

</html>