<?php if (!empty($errors)): foreach ($errors as $error): ?>
    <div class="alert alert-danger">
        <?php echo $error->getMessage(); ?>
    </div>
<?php endforeach; endif; ?>

<div class="panel panel-default">
    <div class="panel-heading"><h1 style="text-align: center;">Добавление новой записи</h1></div>
    <div class="panel-heading"><a href="/index<?php echo '?page='.$page.'&sort='.$sort; ?>" class="js-back">&laquo; назад</a></div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <form action="/create/" enctype="multipart/form-data" method="POST">
            <div class="panel-heading">Заголовок:</div>
            <input type="text" name="title" value="<?php if(!empty($_POST["title"])) { echo $_POST["title"]; } ?>" />
            <hr/>
            <div class="panel-heading">Текст:</div>
            <textarea name="content" cols="40" rows="8"><?php if(!empty($_POST["title"])) { echo $_POST["content"]; } ?></textarea>
            <hr/>
            <div class="panel-heading">Изображение:</div>
            <input type="file" name="img" /> <br />
            <input type="submit" name="submit" value="Добавить" />
            <br/><br/>
        </form>
    </div>
</div>