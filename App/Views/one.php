<div class="panel panel-default">
    <div class="panel-heading"><h1 style="text-align: center;"><?php echo $article->title; ?></h1></div>

    <div class="panel-heading"><a href="/index?page=<?php echo $page.'&sort='.$sort; ?>" class="js-back">&laquo; назад</a></div> <br />

    <div class="panel-heading"><?php echo $article->date; ?></div>

    <div class="panel-body">
        <?php if (!empty($article->img)): ?>
            <img src="<?php echo 'App/Views/img/'.$article->img; ?>" alt="img" class="img-responsive" height="50%" width="50%" />
        <?php endif; ?>

        <?php echo $article->content; ?>
    </div>
</div>