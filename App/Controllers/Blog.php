<?php

namespace App\Controllers;

use App\MultiException;
use App\View;

class Blog
{

    protected $view;
    public $sort;

    public function __construct()
    {
        $this->view = new View();
        $this->sort = !empty($_GET["sort"]) ? $_GET["sort"] : "asc";
    }

    public function action($action, $e = '')
    {
        $methodName = 'action' . $action;
        return $this->$methodName($e);
    }

    protected function actionIndex($sort = "asc")
    {
        $page = !empty($_GET["page"]) ? (int)$_GET["page"] : 1;
        $per_page = 5;
        $total_count = \App\Models\Blog::countAll();
        $pagination = new \App\Pagination($page, $per_page, $total_count[0]->count);

        $this->view->title = "Блог";
        $this->view->posts = \App\Models\Blog::findAll($pagination->per_page, $pagination->offset(), $this->sort);
        $this->view->sort = $this->sort;
        $this->view->page = $page;
        $this->view->pagination = $pagination;

        $this->view->display(__DIR__ . '/../Views/index.php');
    }

    protected function actionOne()
    {
        $this->view->title = "Запись";
        $id = (int)$_GET["id"];

        $this->view->article = \App\Models\Blog::findById($id);
        $this->view->sort = $this->sort;
        $this->view->page = !empty($_GET["page"]) ? (int)$_GET["page"] : 1;

        $this->view->display(__DIR__ . '/../Views/one.php');
    }

    protected function actionException($e)
    {
        $this->view->title = "Ошибка";
        $this->view->exception = $e;
        $this->view->display(__DIR__ . '/../Views/exception.php');
    }

    protected function actionCreate()
    {
        try {
            $article = new \App\Models\Blog();
            if ($article->fill($_POST) !== false) {
                $article->save();

                header('Location: /index');
            }
        } catch (MultiException $e) {
            $this->view->errors = $e;
        }

        $this->view->title = "Добавить новую запись";
        $this->view->sort = $this->sort;
        $this->view->page = !empty($_GET["page"]) ? (int)$_GET["page"] : 1;

        $this->view->display(__DIR__ . '/../Views/create.php');
    }

    protected function actionUpdate()
    {
        try {
            $article = new \App\Models\Blog();
            if ($article->fill($_POST) !== false) {
                $article->update();
                header('Location: /index');
            }
        } catch (MultiException $e) {
            $this->view->errors = $e;
        }

        $this->view->title = "Редактировать запись";
        $this->view->sort = $this->sort;
        $this->view->page = !empty($_GET["page"]) ? (int)$_GET["page"] : 1;
        $this->view->data = $article::findById($_GET['id']);

        $this->view->display(__DIR__ . '/../Views/update.php');
    }

    protected function actionDelete()
    {
        $article = new \App\Models\Blog();
        if ($article->delete()) {
            $img = $article::findById($_GET["id"])->img;
            unlink("App/Views/img/".$img);
            $page = !empty($_GET["page"]) ? (int)$_GET["page"] : 1;
            header("Location: /index?page=".$page."&sort=".$this->sort);
        } else {
            throw new \App\Exceptions\Db("Ссылка на несуществующий объект");
        }
    }

}