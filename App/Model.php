<?php

namespace App;

abstract class Model {

    const TABLE = '';

    public $id;

    public static function findAll($per_page, $offset, $sort) {
        $db = Db::instance();
        $sql = 'SELECT * FROM ' . static::TABLE;
        $sql .= ' ORDER BY date';
        if ($sort == "desc") {
            $sql .= ' DESC';
        } elseif ($sort == "asc") {
            $sql .= ' ASC';
        }
        $sql .= ' LIMIT ' . $per_page . ' OFFSET ' . $offset;
        return $db->queryEach($sql, [], static::class);
    }

    public static function findById($id) {
        $db = Db::instance();
        $row = $db->query('SELECT * FROM ' . static::TABLE . ' WHERE id = :id', [':id' => $id], static::class)[0];
        if(!empty($row)) {
            return $row;
        } else {
            return false;
        }
    }

    public static function countAll() {
        $db = Db::instance();
        return $db->query('SELECT COUNT(*) AS count FROM ' . static::TABLE, [], static::class);
    }

    public function isNew() {
        return empty($this->id);
    }

    public function insert() {
        if (!$this->isNew()) {
            return;
        }

        $columns = [];
        $values = [];
        foreach ($this as $k => $v) {  // Проходимся по всем свойствам текущего объекта
            if ('id' == $k) {
                continue;  // Пропускаем id (начинаем следующую итерацию)
            }
            $columns[] = $k;  // Имена столбцов = имена свойств объекта
            $values[':'.$k] = $v;  // Имена подстановок = значения свойств объекта
        }

        $sql = 'INSERT INTO ' . static::TABLE . ' (' . implode(', ', $columns);
        $sql .= ') VALUES (';
        $sql .= implode(', ', array_keys($values)) . ')';  // Вставляем в виде строки ключи массива $values (подстановки)
        $db = Db::instance();
        if ($db->execute($sql, $values)) {  // Передаем sql-запрос и значения массива $values (значения подстановок)
            $this->id = $db->lastId();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        $values = [];
        $attribute_pairs = [];
        foreach($this as $k => $v) {
            $values[':'.$k] = $v;
            $attribute_pairs[] = $k . '=:' . $k;
        }

        $sql = 'UPDATE ' . static::TABLE . ' SET ' . implode(', ', $attribute_pairs);
        $sql .= ' WHERE id = ' . $values[':id'];
        $db = Db::instance();
        $db->execute($sql, $values);
        return ($db->rowCount() == 1) ? true : false;
    }

    public function save() {
        return empty($this->id) ? $this->insert() : $this->update();
    }

    public function delete() {
        $this->id = (int)$_GET["id"];
        $sql = "DELETE FROM " . static::TABLE;
        $sql .= " WHERE id = :id";
        $sql .= " LIMIT 1";
        $db = Db::instance();
        $db->execute($sql, [":id" => $this->id]);
        return ($db->rowCount() == 1) ? true : false;
    }

}