<?php

namespace App;

trait Singleton {

    protected static $instance;

    protected function __construct() {}

    public static function instance() {
        if (static::$instance === null) {
            $class_name = get_called_class();
            static::$instance = new $class_name;
        }
        return static::$instance;
    }

}