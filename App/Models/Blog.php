<?php

namespace App\Models;

use App\Model;
use App\MultiException;

class Blog extends Model
{

    const TABLE = 'blog';

    public $title;
    public $content;
    public $date;
    public $img;

    public function fill(array $data)
    {
        if (isset($data["submit"])) {
            $e = new MultiException();

            if (empty($data["title"])) {
                $e[] = new \Exception("Пустой заголовок");
            }

            if (empty($data["content"])) {
                $e[] = new \Exception("Пустой текст поста");
            }

            if (empty($_FILES["img"])) {
                $e[] = new \Exception("Не удалось загрузить изображение. Попробуйте еще раз");
            } else {
                $tmp_file = $_FILES["img"]["tmp_name"];
                $target_file = basename($_FILES["img"]["name"]);
                $upload_dir = "App/Views/img";
                move_uploaded_file($tmp_file, $_SERVER['DOCUMENT_ROOT']."/".$upload_dir."/".$target_file);
            }

            if (count($e) != 0) {
                throw $e;
            }

            if (strstr($_SERVER['REQUEST_URI'], 'update/') == 'update/') {
                $this->id = $data["id"];
            }
            $this->title = trim($data["title"]);
            $this->content = trim($data["content"]);
            $this->date = (string)date("Y-m-d H:i:s");
            if ($_FILES["img"]["size"] > 0) {
                $this->img = $target_file;
            } else if (!empty($data["img_old"])) {
                $this->img = $data["img_old"];
            } else {
                $this->img = '';
            }
            
            return true;
        } else {
            return false;
        }
    }

}