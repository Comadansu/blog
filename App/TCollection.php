<?php

namespace App;


trait TCollection
{

    protected $data = [];

    // Проверяет, существует ли в объекте элемент с заданным ключом
    // (исполняется, когда используется функция isset() или функция empty() для объекта, реализующего интерфейс ArrayAccess.)
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    // Получает значение по заданному ключу
    public function offsetGet($offset)
    {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    // Устанавливает значение (и по заданному ключу, и при отсутствии ключа)
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    // Удаляет элемент с заданным ключом
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    // Возвращает текущий элемент
    public function current()
    {
        return current($this->data);
    }

    // Переходит к следующему элементу
    public function next()
    {
        return next($this->data);
    }

    // Возвращает ключ текущего элемента
    public function key()
    {
        return key($this->data);
    }

    // Проверка корректности позиции
    public function valid()
    {
        return current($this->data) !== false;
    }

    // Возвращает итератор на первый элемент
    public function rewind()
    {
        reset($this->data);
    }

    public function count()
    {
        return count($this->data);
    }

}