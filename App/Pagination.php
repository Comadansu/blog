<?php

namespace App;

class Pagination {

	public $current_page;  // Текущая страница
	public $per_page;  	   // Кол-во записей на страницу
	public $total_count;   // Общее кол-во записей

	public function __construct($page = 1, $per_page = 20, $total_count = 0) {
		$this->current_page = (int)$page;
		$this->per_page = (int)$per_page;
		$this->total_count = (int)$total_count;
	}

	// Смещение записей
	public function offset() {
		// Страница 1 будет иметь смещение = 0    (1-1) * 20
		return ($this->current_page - 1) * $this->per_page;
	}

	// Общее кол-во страниц
	public function total_pages() {
		return ceil($this->total_count / $this->per_page);
	}

	// Предыдущая страница
	public function previous_page() {
		return $this->current_page - 1;
	}

	// Следующая страница
	public function next_page() {
		return $this->current_page + 1;
	}

	// Проверка на существование предыдущей страницы
	public function has_previous_page() {
		return $this->previous_page() >= 1 ? true : false;
	}

	// Проверка на существование следующей страницы
	public function has_next_page() {
		return $this->next_page() <= $this->total_pages() ? true : false;
	}

}


?>