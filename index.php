<?php
require_once(__DIR__ . '/autoload.php');

$url = $_SERVER["REQUEST_URI"];
$parts = explode("/", $url);

$ctrl = $parts[0] ?: "Blog";
if (strstr($parts[1], "?")) {
    $action = strstr($parts[1], "?", true) ?: "Index";
} else {
    $action = $parts[1] ?: "Index";
}

try {
    $ctrlClass = '\App\Controllers\\' . ucfirst($ctrl);
    $controller = new $ctrlClass();

    $controller->action($action);
} catch(\App\Exceptions\Core $e) {
    $e = "Возникло исключение приложения: " . $e->getMessage();

    $controller = new $ctrlClass();
    $action = "Exception";
    $controller->action($action, $e);
} catch(\App\Exceptions\Db $e) {
    $e = "Ошибка в работе с базой данных: " . $e->getMessage();

    $controller = new $ctrlClass();
    $action = "Exception";
    $controller->action($action, $e);
}